.. test-233-with-hyphen documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

test-233-with-hyphen documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2

   getting-started
   commands



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
